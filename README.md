# The BINGWA repository has moved #
**27/06/2020**

Due to the subsetting of mercurial support by bitbucket.org, I've moved the BINGWA codebase to:

http://code.enkre.net/bingwa


